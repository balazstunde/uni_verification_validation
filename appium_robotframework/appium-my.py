import unittest
import time
from appium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

class MyFirstAppiumTest(unittest.TestCase):
    def setUp(self):
        desire_cap = {}
        desire_cap['platformName'] = "Android"
        desire_cap['platformVersion'] = '8.1.0'
        desire_cap['udid'] = 'emulator-5554'
        desire_cap['deviceName'] = 'Android Emulator'
        desire_cap['no-reset'] = 'true'
        desire_cap['full-reset'] = 'false'
        desire_cap['app'] = 'C:\\Users\\balaz\\Desktop\\egyetem\\hatodik\\ver_val\\szeminarium\\test_debug\\21 Days_v1.07_apkpure.com.apk'
        self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desire_cap)

    def test_add_challenge_from_existing_challanges_and_modify_the_starting_date(self):
        try:
            wait = WebDriverWait(self.driver, 20)
            close = wait.until(EC.element_to_be_clickable((By.XPATH, "//android.widget.Button[@resource-id='com.marcinrabiej.twentyonedays:id/closeButton']")))
            close.click()
            element = wait.until(EC.element_to_be_clickable((By.ID, "no_challenges_label")))
            element.click()
            challenge_browser = wait.until(EC.element_to_be_clickable((By.ID, "startChallengeBrowserButton")))
            challenge_browser.click()
            my_challenge = wait.until(EC.element_to_be_clickable((By.XPATH, "//android.widget.LinearLayout[@index='3']")))
            my_challenge.click()
            change_date = wait.until(EC.element_to_be_clickable((By.ID, "startDatePickerButton")))
            change_date.click()
            date_may_24 = wait.until(EC.element_to_be_clickable((By.XPATH, "//android.view.View[@text='24']")))
            date_may_24.click()
            ok = wait.until(EC.element_to_be_clickable((By.XPATH, "//android.widget.Button[@text='OK']")))
            ok.click()
            save =  wait.until(EC.element_to_be_clickable((By. ID, "action_save_text")))
            save.click()
            return True
        except NoSuchElementException:
            self.fail('test_add_challenge_from_existing_challanges_and_modify_the_starting_date')

    def test_add_a_challenge_then_signt_it_as_failed_but_you_try_it_again(self):
        try:
            wait = WebDriverWait(self.driver, 20)
            close = wait.until(EC.element_to_be_clickable((By.XPATH, "//android.widget.Button[@resource-id='com.marcinrabiej.twentyonedays:id/closeButton']")))
            close.click()
            element = wait.until(EC.element_to_be_clickable((By.ID, "no_challenges_label")))
            element.click()
            challenge = wait.until(EC.element_to_be_clickable((By.ID, "challengeNameEditText")))
            challenge.send_keys('I will exercise every day!')
            save =  wait.until(EC.element_to_be_clickable((By. ID, "action_save_text")))
            save.click()
            today = wait.until(EC.element_to_be_clickable((By.ID, "todayLabel")))
            today.click()
            made_it = wait.until(EC.element_to_be_clickable((By.ID, "button_selectFailure")))
            made_it.click()
            ok = wait.until(EC.element_to_be_clickable((By.ID, "button_ok")))
            ok.click()
            later = wait.until(EC.element_to_be_clickable((By.ID, "retryButton")))
            later.click()
            #save =  wait.until(EC.element_to_be_clickable((By. ID, "action_save_text")))
            #save.click()
            #navigate_up = wait.until(EC.element_to_be_clickable((By.XPATH, "//android.widget.ImageButton[@content-desc='Navigate up']")))
            #navigate_up.click()
            return True
        except NoSuchElementException:
            self.fail('test_add_a_challenge_then_signt_it_as_failed_but_you_try_it_again')

    def test_add_a_challenge_then_signt_it_as_failed_but_you_will_do_it_later_check_it_in_failures_delete_it(self):
        try:
            wait = WebDriverWait(self.driver, 20)
            close = wait.until(EC.element_to_be_clickable((By.XPATH, "//android.widget.Button[@resource-id='com.marcinrabiej.twentyonedays:id/closeButton']")))
            close.click()
            element = wait.until(EC.element_to_be_clickable((By.ID, "no_challenges_label")))
            element.click()
            challenge = wait.until(EC.element_to_be_clickable((By.ID, "challengeNameEditText")))
            challenge.send_keys('I will exercise every day!')
            save =  wait.until(EC.element_to_be_clickable((By. ID, "action_save_text")))
            save.click()
            today = wait.until(EC.element_to_be_clickable((By.ID, "todayLabel")))
            today.click()
            made_it = wait.until(EC.element_to_be_clickable((By.ID, "button_selectFailure")))
            made_it.click()
            ok = wait.until(EC.element_to_be_clickable((By.ID, "button_ok")))
            ok.click()
            later = wait.until(EC.element_to_be_clickable((By.ID, "laterButton")))
            later.click()
            navigate_up = wait.until(EC.element_to_be_clickable((By.XPATH, "//android.widget.ImageButton[@content-desc='Navigate up']")))
            navigate_up.click()
            failures = wait.until(EC.element_to_be_clickable((By.XPATH, "//android.widget.TextView[@text='FAILED']")))
            failures.click()
            failed_challenge = wait.until(EC.element_to_be_clickable((By.ID, "challengeInfoFrame")))
            failed_challenge.click()
            delete = wait.until(EC.element_to_be_clickable((By.ID, "action_delete")))
            delete.click()
            continue_delete = wait.until(EC.element_to_be_clickable((By.XPATH, "//android.widget.Button[@text='CONTINUE']")))
            continue_delete.click()
            return True
        except NoSuchElementException:
            self.fail('test_add_a_challenge_then_signt_it_as_failed_but_you_will_do_it_later_check_it_in_failures_delete_it')

    def test_add_a_challenge_then_edit_it(self):
        try:
            wait = WebDriverWait(self.driver, 20)
            close = wait.until(EC.element_to_be_clickable((By.XPATH, "//android.widget.Button[@resource-id='com.marcinrabiej.twentyonedays:id/closeButton']")))
            close.click()
            element = wait.until(EC.element_to_be_clickable((By.ID, "no_challenges_label")))
            element.click()
            challenge = wait.until(EC.element_to_be_clickable((By.ID, "challengeNameEditText")))
            challenge.send_keys('I will exercise every day!')
            save =  wait.until(EC.element_to_be_clickable((By. ID, "action_save_text")))
            save.click()
            edit = wait.until(EC.element_to_be_clickable((By.XPATH, "//android.widget.TextView[@content-desc='Edit']")))
            edit.click()
            challenge = wait.until(EC.element_to_be_clickable((By.ID, "challengeNameEditText")))
            challenge.send_keys('I will exercise every day two times!')
            save =  wait.until(EC.element_to_be_clickable((By. ID, "action_save_text")))
            save.click()
            return True
        except NoSuchElementException:
            self.fail('test_add_a_challenge_then_edit_it')


    def test_add_a_challenge_then_delete_it(self):
        try:
            wait = WebDriverWait(self.driver, 20)
            close = wait.until(EC.element_to_be_clickable((By.XPATH, "//android.widget.Button[@resource-id='com.marcinrabiej.twentyonedays:id/closeButton']")))
            close.click()
            element = wait.until(EC.element_to_be_clickable((By.ID, "no_challenges_label")))
            element.click()
            challenge = wait.until(EC.element_to_be_clickable((By.ID, "challengeNameEditText")))
            challenge.send_keys('I will exercise every day!')
            save =  wait.until(EC.element_to_be_clickable((By. ID, "action_save_text")))
            save.click()
            delete = wait.until(EC.element_to_be_clickable((By.ID, "action_delete")))
            delete.click()
            continue_delete = wait.until(EC.element_to_be_clickable((By.XPATH, "//android.widget.Button[@text='CONTINUE']")))
            continue_delete.click()
            return True
        except NoSuchElementException:
            self.fail('test_add_a_challenge_then_delete_it')

    def test_change_notification_sound_and_turn_off_vibration(self):
        try:
            wait = WebDriverWait(self.driver, 20)
            close = wait.until(EC.element_to_be_clickable((By.XPATH, "//android.widget.Button[@resource-id='com.marcinrabiej.twentyonedays:id/closeButton']")))
            close.click()
            settings = wait.until(EC.element_to_be_clickable((By.ID, "action_settings")))
            settings.click()
            sound = wait.until(EC.element_to_be_clickable((By.XPATH, "//android.widget.TextView[@text='Notification sound']")))
            sound.click()
            select_a_sound = wait.until(EC.element_to_be_clickable((By.XPATH, "//android.widget.CheckedTextView[@text='Aldebaran']")))
            select_a_sound.click()
            ok = wait.until(EC.element_to_be_clickable((By.ID, "button1")))
            ok.click()
            vibration = wait.until(EC.element_to_be_clickable((By.XPATH, "//android.widget.TextView[@text='Vibrations']")))
            vibration.click()
            navigate_up = wait.until(EC.element_to_be_clickable((By.XPATH, "//android.widget.ImageButton[@content-desc='Navigate up']")))
            navigate_up.click()
            return True
        except NoSuchElementException:
            self.fail('test_change_notification_sound_and_turn_off_vibration')


    def test_turn_off_notifications(self):
        try:
            wait = WebDriverWait(self.driver, 20)
            close = wait.until(EC.element_to_be_clickable((By.XPATH, "//android.widget.Button[@resource-id='com.marcinrabiej.twentyonedays:id/closeButton']")))
            close.click()
            settings = wait.until(EC.element_to_be_clickable((By.ID, "action_settings")))
            settings.click()
            switch = wait.until(EC.element_to_be_clickable((By.ID, "switch_widget")))
            switch.click()
            navigate_up = wait.until(EC.element_to_be_clickable((By.XPATH, "//android.widget.ImageButton[@content-desc='Navigate up']")))
            navigate_up.click()
            return True
        except NoSuchElementException:
            self.fail('test_turn_off_notifications')
        
    def test_add_new_challenge(self):
        try:
            wait = WebDriverWait(self.driver, 20)
            close = wait.until(EC.element_to_be_clickable((By.XPATH, "//android.widget.Button[@resource-id='com.marcinrabiej.twentyonedays:id/closeButton']")))
            close.click()
            element = wait.until(EC.element_to_be_clickable((By.ID, "no_challenges_label")))
            element.click()
            challenge = wait.until(EC.element_to_be_clickable((By.ID, "challengeNameEditText")))
            challenge.send_keys('I will exercise every day!')
            save =  wait.until(EC.element_to_be_clickable((By. ID, "action_save_text")))
            save.click()
            return True
        except NoSuchElementException:
            self.fail('test_add_new_challenge')
 
    def test_after_add_new_challenge_sign_it_as_done(self):
        try:
            wait = WebDriverWait(self.driver, 20)
            close = wait.until(EC.element_to_be_clickable((By.XPATH, "//android.widget.Button[@resource-id='com.marcinrabiej.twentyonedays:id/closeButton']")))
            close.click()
            element = wait.until(EC.element_to_be_clickable((By.ID, "no_challenges_label")))
            element.click()
            challenge = wait.until(EC.element_to_be_clickable((By.ID, "challengeNameEditText")))
            challenge.send_keys('I will exercise every day!')
            save =  wait.until(EC.element_to_be_clickable((By. ID, "action_save_text")))
            save.click()
            today = wait.until(EC.element_to_be_clickable((By.ID, "todayLabel")))
            today.click()
            made_it = wait.until(EC.element_to_be_clickable((By.ID, "button_selectSuccess")))
            made_it.click()
            ok = wait.until(EC.element_to_be_clickable((By.ID, "button_ok")))
            ok.click()
            return True
        except NoSuchElementException:
            self.fail('test_after_add_new_challenge_sign_it_as_done')

    def test_help(self):
        try:
            wait = WebDriverWait(self.driver, 20)
            element = wait.until(EC.element_to_be_clickable((By.XPATH, "//android.widget.TextView[@text='2']")))
            #currently_waiting_for = wait.until(EC.element_to_be_clickable(By.XPATH, "//android.widget.TextView[@text='2']"))
            element.click()
            int_3 = self.driver.find_element_by_xpath("//android.widget.TextView[@text='3']")
            int_3.click()
            int_4 = self.driver.find_element_by_xpath("//android.widget.TextView[@text='4']")
            int_4.click()
            int_5 = self.driver.find_element_by_xpath("//android.widget.TextView[@text='5']")
            int_5.click()
            int_6 = self.driver.find_element_by_xpath("//android.widget.TextView[@text='6']")
            int_6.click()
            int_7 = self.driver.find_element_by_xpath("//android.widget.TextView[@text='7']")
            int_7.click()
            el_view = self.driver.find_element_by_xpath("//android.widget.Button[@resource-id='com.marcinrabiej.twentyonedays:id/closeButton']")
            el_view.click()
            return True
        except NoSuchElementException:
            self.fail('test_help')

    #def tearDown(self):
        #self.driver.quit()

if __name__ == "__main__":
    unittest.main(verbosity=1)
