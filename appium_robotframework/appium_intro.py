import unittest
import time

from appium import webdriver
from selenium.common.exceptions import NoSuchElementException


class MyFirstAppiumTest(unittest.TestCase):
    def setUp(self):
        # configurations
        desire_capabilities = \
            {
                'platformName': 'Android',
                'platform_version': '8.1.0',  # make sure this matches the version of the os you are testing
                'udid': 'emulator-5554',  # got with AppData\Local\Android\Sdk\platform-tools>adb devices command
                'deviceName': 'Android Emulator',  # or Android if running on normal device

                'app': 'TODO: FILL THIS WITH THE ABSOLUTE PATH TO THE APK YOU WANT TO TEST '
                       'ex.: C:\\Users\\wow\\ApiDemos-debug.apk'

            }
        # Connect to appium server running on local machine
        self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desire_capabilities)
        self.driver.implicitly_wait(10)  # if an element is not found in 10 secs, fail

    def test_demo(self):
        a = 2
        self.assertEqual(a, 2)

    def test_view_click(self):
        try:
            el_view = self.driver.find_element_by_android_uiautomator('text("Views")')
            el_view.click()

            time.sleep(2)
            return True

        except NoSuchElementException:
            self.fail('Views element not found')

    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    print("Starting appium_intro...")
    unittest.main(verbosity=1)
