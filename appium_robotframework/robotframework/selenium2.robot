*** Settings ***
Library  Selenium2Library

*** Variables ***
${SHOP}    Lottozó
${NAME}    Abrakadabra
${EMAIL}   abrakadabra@gmail.com
${PHONE}   123456789
${MESSAGE}  Koszi

*** Test Cases ***
Hagyomanyok es kapcsolat bongeszese
    Megnyitjuk az adott oldalt
    Reklam figyelmen kivul hagyasa
    A manufaktura kapcsolatainak elolvasasa
    A manufaktura hagyomanyainak elolvasasa
    Bongeszo bezarasa

Adatvedelem bongeszese
    Megnyitjuk az adott oldalt
    Reklam figyelmen kivul hagyasa
    Adatkezelesi tajekoztato megnyitasa
    A megrendelesnel a szemelyekre vonatkozo informaciok elolvasasa
    A kapcsolatfelvetelnel a szemelyekre vonatkozo informaciok elolvasasa
    A nyeremenyjateknal a szemelyekre vonatkozo informaciok elolvasasa
    A sutikre vonatkozo Adatkezelesi tajekoztato elolvasasa
    A szerzodeses partnerek kapcsan kezelt szemelyes adatokra vonatkozo tajekoztatao elolvasasa
    Bongeszo bezarasa
 
Latogatokozpont informaciok megnezese
    Megnyitjuk az adott oldalt
    Reklam figyelmen kivul hagyasa
    Belepunk a latogatokozpontba
    Kitoltjuk a formot
    Bongeszo bezarasa
 
Sorvasarlas
    Megnyitjuk az adott oldalt
    Reklam figyelmen kivul hagyasa
    Megnyitjuk a sor shopot
    Elmultam 18 eves
    Harom vadmalna sor vasarlasa
    Bongeszo bezarasa
 
Manufaktura hirek olvasasa
    Megnyitjuk az adott oldalt
    Reklam figyelmen kivul hagyasa
    A manufaktura hirek bongeszese
    Bongeszo bezarasa
 
Manufaktura attekintese kepekkel 
    Megnyitjuk az adott oldalt
    Reklam figyelmen kivul hagyasa
    Manufaktura ahogyan megvasaroltak es videok megnezese
    Bongeszo bezarasa

Nezelodes az ajandekboltban
    Megnyitjuk az adott oldalt
    Reklam figyelmen kivul hagyasa
    Bongeszes az ajandekboltban
    Bongeszo bezarasa

Nezzuk meg a reklamot
    Megnyitjuk az adott oldalt
    Reklam megnezese es szabalyok elolvasasa
    Bongeszo bezarasa

Terkepen valo bongeszes szamokkal
    Megnyitjuk az adott oldalt
    Reklam figyelmen kivul hagyasa
    Terkep megnyitasa es bongeszese
    Bongeszo bezarasa

Terkepen valo bongeszes inputtal
    Megnyitjuk az adott oldalt
    Reklam figyelmen kivul hagyasa
    Terkep megnyitasa es bongeszese inputtal
    Bongeszo bezarasa

*** Keywords ***
Megnyitjuk az adott oldalt
    Open Browser    https://csikisor.hu/    browser=chrome
    Maximize Browser Window

Reklam megnezese es szabalyok elolvasasa
    Click Element    //html//body//div[7]//div//a
    Sleep       3s
    Click Element    //*[@id="post-56339"]/h2[1]/a

Reklam figyelmen kivul hagyasa
    Click Element    //a[contains(@title, 'Close')] 

Bongeszo bezarasa
    Close Browser

Terkep megnyitasa es bongeszese
    Sleep       1s
    Click Element    //*[@id="menu-main-menu-1"]/li[7]/a/span
    Sleep       2s
    Click Element    //*[@id="map"]/div/div/div[1]/div[3]/div/div[3]/div[28]
    Sleep       1s
    Click Element    //*[@id="map"]/div/div/div[1]/div[3]/div/div[3]/div[13]
    Sleep       1s
    Click Element    //*[@id="map"]/div/div/div[1]/div[3]/div/div[3]/div[7]

Terkep megnyitasa es bongeszese inputtal
    Sleep       1s
    Click Element    //*[@id="menu-main-menu-1"]/li[7]/a/span
    Sleep       2s
    Input Text    //input[@id='search']    ${SHOP}
    Sleep       2s
    Click Element  //*[@id="results-list"]/li[3513]/h5
    Sleep       1s

Bongeszes az ajandekboltban
    Sleep       1s
    Click Element  //*[@id="menu-main-menu-1"]/li[6]/a/span
    Sleep       1s
    Click Element  //*[@id="post-3217"]/div/div/div/div/div/div/ul/li[3]/a[1]/img
    Sleep       1s
    Click Element  //*[@id="product-378"]/div[2]/form/button
    Sleep       1s
    Go Back
    Go Back
    Sleep       1s
    Execute JavaScript    window.scrollTo(0,2000)
    Sleep       1s
    CLick Element  //*[@id="post-3217"]/div/div/div/div/div/div/ul/li[18]/a[1]/img
    Sleep       1s
    Click Element  //*[@id="product-361"]/div[2]/form/button
    Sleep       1s

Manufaktura ahogyan megvasaroltak es videok megnezese
    Mouse Over  //*[@id="menu-main-menu-1"]/li[5]/a/span[1]
    Sleep       1s
    Click Element   //*[@id="menu-main-menu-1"]/li[5]/ul/li[2]/a
    Sleep       1s
    Execute JavaScript    window.scrollTo(0,800)
    Sleep       1s 
    Click Element   //*[@id="fancygallery-1"]/div[1]/div[2]/a/img
    Sleep       1s 
    Execute JavaScript    window.scrollTo(0,1500)
    Sleep       1s 
    Click Element   //*[@id="fancygallery-1"]/div[3]/a
    Sleep       1s 
    Click Element  //*[@id="fancygallery-1"]/div[1]/div[10]/a/img
    Sleep       1s
    Execute JavaScript    window.scrollTo(0,1500)
    Sleep       1s
A manufaktura hirek bongeszese
    Mouse Over  //*[@id="menu-main-menu-1"]/li[5]/a/span[1]
    Sleep       1s
    Click Element  //*[@id="menu-main-menu-1"]/li[5]/ul/li[3]/a
    Sleep       1s
    Execute JavaScript    window.scrollTo(0,500)
    Sleep       1s
    Click Element  //*[@id="post-903"]/div[2]/div[1]/h2/a
    Sleep       1s
    Execute JavaScript    window.scrollTo(0,200)
    Sleep       1s
    Execute JavaScript    window.scrollTo(0,200)
    Sleep       1s
    Execute JavaScript    window.scrollTo(0,200)
    Sleep       1s
    Go Back
    Sleep       1s
    Click ELement   //*[@id="post-916"]/div[2]/div[1]/h2/a
    Sleep       1s
    Execute JavaScript     window.scrollTo(0,500)
    Sleep       1s

Megnyitjuk a sor shopot
    Sleep     2s
    Click Element   //*[@id="menu-main-menu-1"]/li[3]/a/span
    Sleep        1s
Elmultam 18 eves
    Sleep     3s
    Click Element  //*[@id="snp-bld-step-1"]/form/div[2]/button

Harom vadmalna sor vasarlasa
    Sleep     1s
    Click Element  //*[@id="main-container"]/div/section/div/div[1]/div/div/div[3]/a/div/div/h4
    Sleep     1s
    Click Element  //*[@id="post-54550"]/div[1]/div[2]/div/div/div/ul/li[2]/a
    Sleep     1s
    Click Element   //*[@id="product-54544"]/div[2]/form/div/button[2]
    Click Element   //*[@id="product-54544"]/div[2]/form/div/button[2]
    Click ELement   //*[@id="product-54544"]/div[2]/form/button

Belepunk a latogatokozpontba
    Sleep  2s
    Click Element   //*[@id="menu-main-menu-1"]/li[2]/a/span

Kitoltjuk a formot
    Sleep  2s
    Input Text    //input[@name='your-name']    ${SHOP}
    Input Text    //input[@name='your-email']    ${EMAIL}
    Input Text    //input[@name='telefon']    ${EMAIL}
    Input Text    //*[@id="wpcf7-f4-p22-o1"]/form/p[4]/span/textarea   ${MESSAGE}
    Click Element  //*[@id="wpcf7-f4-p22-o1"]/form/p[5]/span/span/span/input
    Sleep  2s

Adatkezelesi tajekoztato megnyitasa
    Sleep  2s
    Click Element  //*[@id="cmo-footer"]/div[1]/div/div[3]/div/a

A megrendelesnel a szemelyekre vonatkozo informaciok elolvasasa
    Execute JavaScript     window.scrollTo(0,0)
    Sleep  2s
    Click Element  //*[@id="post-48972"]/div[1]/div/div/div/div/ol/li[1]/a

A kapcsolatfelvetelnel a szemelyekre vonatkozo informaciok elolvasasa
    Execute JavaScript     window.scrollTo(0,0)
    Sleep  2s
    Click Element  //*[@id="post-48972"]/div[1]/div/div/div/div/ol/li[2]/a

A nyeremenyjateknal a szemelyekre vonatkozo informaciok elolvasasa
    Execute JavaScript     window.scrollTo(0,0)
    Sleep  2s
    Click Element  //*[@id="post-48972"]/div[1]/div/div/div/div/ol/li[3]/a

A sutikre vonatkozo Adatkezelesi tajekoztato elolvasasa
    Execute JavaScript     window.scrollTo(0,0)
    Sleep  2s
    Click Element  //*[@id="post-48972"]/div[1]/div/div/div/div/ol/li[4]/a

A szerzodeses partnerek kapcsan kezelt szemelyes adatokra vonatkozo tajekoztatao elolvasasa
    Execute JavaScript     window.scrollTo(0,0)
    Sleep  2s
    Click Element  //*[@id="post-48972"]/div[1]/div/div/div/div/ol/li[5]/a

A manufaktura kapcsolatainak elolvasasa
    Sleep      1s
    Mouse Over  //*[@id="menu-main-menu-1"]/li[5]/a/span[1]
    Sleep       1s
    Click Element   //*[@id="menu-main-menu-1"]/li[5]/ul/li[4]/a
    Sleep       1s
    Execute JavaScript    window.scrollTo(0,800)
    Sleep       1s 

A manufaktura hagyomanyainak elolvasasa
    Execute JavaScript    window.scrollTo(0,0)   
    Sleep       1s 
    Sleep      1s
    Mouse Over  //*[@id="menu-main-menu-1"]/li[5]/a/span[1]
    Sleep       1s
    Click Element   //*[@id="menu-main-menu-1"]/li[5]/ul/li[1]/a
    Sleep       1s
    Execute JavaScript    window.scrollTo(0,1000)
    Sleep       1s 
