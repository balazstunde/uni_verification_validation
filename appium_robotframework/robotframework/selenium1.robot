*** Settings ***
Library  Selenium2Library

*** Variables ***
${USERNAME}    user1
${PASSWORD}    pass1

*** Test Cases ***
Regisztraljunk egy felhasznalot
    Megnyitjuk az adott oldalt
    Bejelentkezes
    Adatok ellenorzese
	Close Browser

*** Keywords ***
Megnyitjuk az adott oldalt
    Open Browser    http://thedemosite.co.uk/index.php    browser=chrome
    Maximize Browser Window

Bejelentkezes
    Click Element    //a[contains(text(),'3. Add a User')]
    Input Text    //input[@name='username']    ${USERNAME}
	Input Password    //input[@name='password']    ${PASSWORD}
    Click Element    //input[@value='save']

Adatok ellenorzese
    Element Should Contain  //td[@class='auto-style1']//blockquote//blockquote//blockquote  The username: ${USERNAME}
    Element Should Contain    //td[@class='auto-style1']//blockquote//blockquote//blockquote  The password: ${PASSWORD}
